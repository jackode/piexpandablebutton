//
//  PIExpandableButton.m
//  NewPiki
//
//  Created by Pham Quy on 9/24/15.
//  Copyright © 2015 Pikicast Inc. All rights reserved.
//

#import "PIExpandableButton.h"
#define BOUND(VALUE, UPPER, LOWER)	MIN(MAX(VALUE, LOWER), UPPER)
#define ICON_TITLE_SPACING 6
@interface PIExpandableButton ()
@property (nonatomic, getter=isExpanded) BOOL expanded;
@property (nonatomic, strong) NSMutableDictionary* titleSizeCache;
@end

#pragma mark -
@implementation PIExpandableButton
@synthesize expanded=_expanded;


//------------------------------------------------------------------------------
#pragma mark - Initialization 
//------------------------------------------------------------------------------
- (instancetype) init
{
    self = [super init];
    if(self){
        [self setupDefault];
    }
    return self;
}

//------------------------------------------------------------------------------
- (void) awakeFromNib{
    [super awakeFromNib];
    [self setupDefault];
}

//------------------------------------------------------------------------------
- (void) setupDefault
{
    //TODO: setup default ui appreance setting here
    _expandDuration = 1;
    _titleSizeCache = [NSMutableDictionary dictionary];
    [self setClipsToBounds:YES];
    [self.imageView setContentMode:(UIViewContentModeScaleAspectFill)];
    [self.imageView setClipsToBounds:NO];
    [self.titleLabel setContentMode:(UIViewContentModeLeft)];
    self.titleLabel.alpha = 0.;
}
//------------------------------------------------------------------------------
- (void) expandWithTitle:(NSString *)string duration:(NSTimeInterval)expandDuration
{
    UIControlState currentState = self.state;
    NSString* currentTitle = [[self titleForState:self.state] copy];

    [self setTitle:string forState:currentState];
    [self setExpanded:YES];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self invalidateIntrinsicContentSize];
    BOOL userInteraction = self.userInteractionEnabled;
    [self setUserInteractionEnabled:NO];
    __weak typeof(self) wself = self;
    self.titleLabel.alpha = 0.;
    [UIView
     animateWithDuration:.5
     delay:0
     usingSpringWithDamping:.5
     initialSpringVelocity:.5
     options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionLayoutSubviews
     animations:^{
         self.titleLabel.alpha = 1.;
         [self layoutIfNeeded];
     }completion:^(BOOL finished) {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(expandDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             PIExpandableButton* sSelf  = wself;
             if (sSelf) {
                 [sSelf setExpanded:NO];
                 [sSelf invalidateIntrinsicContentSize];
                 [UIView animateWithDuration:.3 animations:^{
                     sSelf.titleLabel.alpha = 0.;
                     [sSelf layoutIfNeeded];
                     [sSelf setUserInteractionEnabled:userInteraction];
                 }completion:^(BOOL finished) {
                     [wself setTitle:currentTitle forState:currentState];
                 }];
             }
         });
     }];

}
//------------------------------------------------------------------------------
#pragma mark - Overide
//------------------------------------------------------------------------------

- (void) setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];

    if (title) {
        CGSize titleBound = [self boundForTitle:title];
        NSValue* sizeValue = [NSValue valueWithCGSize:titleBound];
        [self.titleSizeCache setObject:sizeValue forKey:@(state)];
    }else{
        [self.titleSizeCache removeObjectForKey:@(state)];
    }
}
//------------------------------------------------------------------------------
- (void) setSelected:(BOOL)selected{
    [self setSelected:selected animated:NO];
}
//------------------------------------------------------------------------------
- (void) setSelected:(BOOL)selected animated:(BOOL) animated
{
    [super setSelected:selected];
    [self layoutIfNeeded];
    if (animated) {
        BOOL userInteraction = self.userInteractionEnabled;
        [self setUserInteractionEnabled:NO];
        __weak typeof(self) wself = self;

        [self setExpanded:YES];
        [self invalidateIntrinsicContentSize];

        self.titleLabel.alpha = 0.;
        [UIView
         animateWithDuration:.5
         delay:0
         usingSpringWithDamping:.5
         initialSpringVelocity:.5
         options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionLayoutSubviews
         animations:^{
             self.titleLabel.alpha = 1.;
             [self layoutIfNeeded];
         }completion:^(BOOL finished) {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_expandDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 PIExpandableButton* sSelf  = wself;
                 if (sSelf) {
                     [sSelf setExpanded:NO];
                     [sSelf invalidateIntrinsicContentSize];
                     [UIView animateWithDuration:.3 animations:^{
                         sSelf.titleLabel.alpha = 0.;
                         [sSelf layoutIfNeeded];
                         [sSelf setUserInteractionEnabled:userInteraction];
                     }];

                 }
             });
         }];
    }
}
//------------------------------------------------------------------------------
- (CGSize) intrinsicContentSize
{
    return [self intrinsicContentSizeForExpandState:self.expanded];
}
//------------------------------------------------------------------------------
- (CGSize) sizeForState:(UIControlState) state
               expanded:(BOOL)expanded
{
    return CGSizeMake(100, self.bounds.size.height);
}
//------------------------------------------------------------------------------
- (CGRect)backgroundRectForBounds:(CGRect)bounds
{
    return bounds;
}
//------------------------------------------------------------------------------
- (CGRect) contentRectForBounds:(CGRect)bounds
{
    return bounds;
}

//------------------------------------------------------------------------------
- (CGRect) titleRectForContentRect:(CGRect)bounds
{
//    if (self.isExpanded) {
//        // TODO: should take into account the expand direction to obtain appropriate frame
//        // This calculation is for horizontal (Right-> Left)

        CGSize validTitleSize = [self validTitleSize];
    CGFloat renderWidth =validTitleSize.width;
//    MIN(self.bounds.size.width
//                                  - self.contentEdgeInsets.left
//                                  - self.contentEdgeInsets.right
//                                  - [self imageRectForContentRect:bounds].size.width,
//                                  validTitleSize.width);

        CGFloat renderHeight = MIN(self.bounds.size.height
                                   - self.contentEdgeInsets.top
                                   - self.contentEdgeInsets.bottom,
                                   validTitleSize.height);

        return  CGRectMake(self.contentEdgeInsets.left + [self imageRectForContentRect:bounds].size.width + ICON_TITLE_SPACING,
                           MAX(self.bounds.size.height/2 - validTitleSize.height/2, 0.),
                           renderWidth,renderHeight);

//    }else{
//        return CGRectZero;
//    }
}

//------------------------------------------------------------------------------
- (CGRect) imageRectForContentRect:(CGRect)bounds
{
    CGSize validIconSize = [self validIconSize];
    if (self.isExpanded)
    {
        // TODO: should take into account the expand direction to obtain appropriate frame
        // This calculation is for horizontal (Right-> Left)

        CGFloat renderWidth = MIN(self.bounds.size.width 
                                  - self.contentEdgeInsets.left
                                  - self.contentEdgeInsets.right,
                                  validIconSize.width);

        CGFloat renderHeight = MIN(self.bounds.size.height
                                   - self.contentEdgeInsets.top
                                   - self.contentEdgeInsets.bottom,
                                   validIconSize.height);

        return  CGRectMake(self.contentEdgeInsets.left,
                           MAX(self.bounds.size.height/2 - validIconSize.height/2, 0.),
                           renderWidth,renderHeight);
    }else{
        // If button is in close state, locate icon at the center
        return  CGRectMake((self.bounds.size.width - validIconSize.width)/2,
                           (self.bounds.size.height - validIconSize.height)/2,
                           validIconSize.width,validIconSize.height);

    }
}

//------------------------------------------------------------------------------
#pragma mark - Private
//------------------------------------------------------------------------------
- (CGSize) intrinsicContentSizeForExpandState:(BOOL) expanded
{
    CGSize intrinsicSize = CGSizeMake(26, 26);
    if (expanded) {
        CGSize titleSize = [self validTitleSize];
        CGFloat width = self.contentEdgeInsets.left + self.iconSize.width + ICON_TITLE_SPACING + titleSize.width + self.contentEdgeInsets.right;
        CGFloat height = self.buttonSize.height;
        return CGSizeMake(width,height);
    }else{
        if ((self.buttonSize.height > 0) && (self.buttonSize.width > 0) ) {
            intrinsicSize = self.buttonSize;
        }
    }
    return intrinsicSize;

}
//------------------------------------------------------------------------------
- (CGSize) validIconSize
{
    // Check if we have valid icon size
    CGSize sizeOfIcon = CGSizeZero;
    if((self.iconSize.height <=0) || (self.iconSize.width <=0)) { // If not
        // Use the size the fit in bound
        CGFloat smallerEdge = MIN(self.bounds.size.height, self.bounds.size.width);
        sizeOfIcon = CGSizeMake(smallerEdge, smallerEdge);
    }else{
        sizeOfIcon = self.iconSize;
    }

    return sizeOfIcon;
}

//------------------------------------------------------------------------------
// TODO:
// 1- Cache the bound (when new title is set, to reduce bound calculation time)
// 2- Support attributed title
- (CGSize) validTitleSize
{
    NSString* title = [self currentTitle];
    UIControlState currentState = self.state;
    NSValue* sizeValue = [self.titleSizeCache objectForKey:@(currentState)];
    if (sizeValue) {
        return [sizeValue CGSizeValue];
    }
    return  [self boundForTitle:title];
}

//------------------------------------------------------------------------------
- (CGSize) boundForTitle:(NSString*) title
{
    CGRect titleBounds = [title boundingRectWithSize:CGSizeMake(INT_MAX, INT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesDeviceMetrics attributes:nil context:nil];
    titleBounds.size.width += 5;//MAX (title.length * 1.8, 10);
    titleBounds.size.height += 5;
    return titleBounds.size;
}

//------------------------------------------------------------------------------
#pragma mark - Setter / Getters
//------------------------------------------------------------------------------
- (void) setCurvatiousness:(CGFloat)curvatiousness
{
    if (_curvatiousness == curvatiousness) {
        return;
    }

    _curvatiousness = BOUND(curvatiousness, 1., 0.);
    if ((self.buttonSize.width > 0) && (self.buttonSize.height > 0)) {
        [self.layer setCornerRadius:(self.buttonSize.height/2* self.curvatiousness)];
    }else{
        [self.layer setCornerRadius:(self.bounds.size.height/2*curvatiousness)];
    }


}
//------------------------------------------------------------------------------
- (void) setButtonSize:(CGSize)buttonSize
{
    _buttonSize = buttonSize;
    if ((buttonSize.width > 0) && (buttonSize.height > 0)) {
        [self.layer setCornerRadius:(self.buttonSize.height/2* self.curvatiousness)];
    }

}
@end
