//
//  AppDelegate.h
//  PIControls
//
//  Created by Pham Quy on 9/24/15.
//  Copyright © 2015 Pikicast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

