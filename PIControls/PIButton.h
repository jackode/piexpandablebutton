//
//  PIButton.h
//  NewPiki
//
//  Created by Pham Quy on 6/16/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PIButton : UIButton
@property (nonatomic, strong) UIColor* highlightBackgroundColor;
@property (nonatomic, strong) UIColor* defaultBackgroundColor;
@property (nonatomic, strong) UIColor* disableBackgroundColor;
@property (nonatomic, strong) UIColor* selectedBackgroundColor;
@end
