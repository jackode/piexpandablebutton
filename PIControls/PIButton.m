//
//  PIButton.m
//  NewPiki
//
//  Created by Pham Quy on 6/16/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "PIButton.h"

#define SET_BG_COLOR_PROP(COLOR_PROPERTY)    if(COLOR_PROPERTY){self.backgroundColor = COLOR_PROPERTY; }

@implementation PIButton

//------------------------------------------------------------------------------
- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];

    if (highlighted) {
        SET_BG_COLOR_PROP(self.highlightBackgroundColor);
    }
    else {
        SET_BG_COLOR_PROP(self.defaultBackgroundColor);
    }
}

//------------------------------------------------------------------------------
- (void) setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    if (enabled) {
        SET_BG_COLOR_PROP(self.defaultBackgroundColor);
    }else {
        SET_BG_COLOR_PROP(self.disableBackgroundColor);
    }
}

//------------------------------------------------------------------------------
- (void) setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        SET_BG_COLOR_PROP(self.selectedBackgroundColor);
    }else {
        SET_BG_COLOR_PROP(self.defaultBackgroundColor);
    }
}
@end
