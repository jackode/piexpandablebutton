//
//  PIExpandableButton.h
//  NewPiki
//
//  Created by Pham Quy on 9/24/15.
//  Copyright © 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PIButton.h"

typedef NS_ENUM(NSInteger, PIButtonExpandDirection) {
    PIButtonExpandDirectionTopToBottom,
    PIButtonExpandDirectionBottomToTop,
    PIButtonExpandDirectionLeftToRight,
    PIButtonExpandDirectionRightToLeft
};

@interface PIExpandableButton : PIButton
@property (nonatomic) PIButtonExpandDirection direction;
@property (nonatomic) CGSize iconSize;
@property (nonatomic) CGSize buttonSize;
@property (nonatomic) CGFloat curvatiousness;
@property (nonatomic) NSTimeInterval expandDuration;

- (void) setSelected:(BOOL)selected animated:(BOOL) animated;
- (void) expandWithTitle:(NSString*) string
                duration:(NSTimeInterval) expandDuration;
@end
