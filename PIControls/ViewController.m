//
//  ViewController.m
//  PIControls
//
//  Created by Pham Quy on 9/24/15.
//  Copyright © 2015 Pikicast. All rights reserved.
//

#import "ViewController.h"
#import "PIExpandableButton.h"

@interface PIControlViewController ()
@property (nonatomic, weak) IBOutlet PIExpandableButton* button;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@end

@implementation PIControlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.button setDefaultBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.5]];
    [self.button setIconSize:CGSizeMake(17, 15)];
    [self.button setButtonSize:CGSizeMake(26, 26)];
    [self.button setContentEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 8)];
    [self.button setCurvatiousness:1];
    [self.button setTitle:@"OFF" forState:(UIControlStateSelected)];
    [self.button setTitle:@"ON" forState:(UIControlStateNormal)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)contentInset:(UISwitch*)sender {
    if (sender.isOn) {
        [self.button setContentEdgeInsets:UIEdgeInsetsMake(0., 10., 0., 0.)];
    }else{
        [self.button setContentEdgeInsets:UIEdgeInsetsMake(0., 0., 0., 0.)];
    }

//    [self.button setNeedsUpdateConstraints];
//    [self.button setNeedsLayout];
//    [self.view setNeedsLayout];
//    [self.view layoutIfNeeded];
}

- (IBAction)titleInset:(UISwitch*)sender {
    if (sender.isOn) {
        [self.button setTitleEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    }else{
        [self.button setTitleEdgeInsets:UIEdgeInsetsMake(0., 0., 0., 0.)];
    }

}

- (IBAction)imageInset:(UISwitch*)sender {
    if (sender.isOn) {
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    }else{
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(0., 0., 0., 0.)];
    }

}

- (IBAction)curve:(UISwitch*)sender {
    if (sender.isOn) {
        [self.button setCurvatiousness:1.];
    }else{
        [self.button setCurvatiousness:0.];
    }
}
- (IBAction)buttonClick:(id)sender {
    [self.button setSelected: !self.button.selected animated:YES];
}
- (IBAction)expanded:(id)sender {
    [self.button expandWithTitle:self.textField.text duration:5.];
}
@end
